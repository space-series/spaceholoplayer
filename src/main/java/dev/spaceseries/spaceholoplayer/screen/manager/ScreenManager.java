package dev.spaceseries.spaceholoplayer.screen.manager;

import dev.spaceseries.spaceholoplayer.screen.cache.FrameCache;
import dev.spaceseries.spaceholoplayer.screen.encoding.FrameEncoder;
import dev.spaceseries.spaceholoplayer.screen.player.IScreenPlayer;
import dev.spaceseries.spaceholoplayer.screen.thread.ScreenThread;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class ScreenManager {

    /**
     * Width of the screen
     */
    @Getter
    private final int width;

    /**
     * Height of the screen
     */
    @Getter
    private final int height;

    /**
     * FPS of the screen
     */
    @Getter
    private final int fps;

    /**
     * Should we loop the video?
     */
    @Getter
    private final boolean loop;

    /**
     * Location of the screen (starting, MIDDLE)
     */
    @Getter
    private final Location screenStart;

    /**
     * The frames cache
     */
    @Getter
    private final FrameCache cache;

    /**
     * The player who started the screen
     */
    @Getter
    private final IScreenPlayer player;

    /**
     * The applicable frame encoder
     */
    @Getter
    private final FrameEncoder encoder;

    /**
     * The screen thread
     */
    @Getter
    private final ScreenThread thread;

    /**
     * Initializes screen manager
     *
     * @param width       The width of the screen
     * @param height      The height of the screen
     * @param fps         The FPS of the screen
     * @param screenStart The location of the screen (MIDDLE)
     * @param player      The player starting the screen
     * @param encoder     The screen encoder
     * @param loop        Should we loop the video
     */
    public ScreenManager(int width, int height, int fps, Location screenStart, IScreenPlayer player, FrameEncoder encoder, boolean loop) {
        this.width = width;
        this.height = height;
        this.fps = fps;
        this.screenStart = screenStart;
        this.loop = loop;
        this.cache = new FrameCache();

        player.setManager(this);
        encoder.setManager(this);
        encoder.load();

        this.player = player;
        this.encoder = encoder;

        this.thread = new ScreenThread(this);
    }

    /**
     * Starts the thread to display images
     */
    public void start() {
        thread.start();
    }

    /**
     * Stops the thread that displays images
     */
    public void stop() {
        thread.setRunning(false);

        // stop for all players watching
        player.getWatchers().forEach(player::stop);
    }

    /**
     * Adds a player who is watching the screen
     *
     * @param user The user
     */
    public void addWatcher(Player user) {
        player.addWatcher(user);
    }

    /**
     * Encodes the video
     */
    public void encode() {
        encoder.encode();
    }

    /**
     * Renders the video
     */
    public void render() {
        player.render();
    }
}
