package dev.spaceseries.spaceholoplayer.screen.player;

import dev.spaceseries.spaceholoplayer.screen.manager.ScreenManager;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.concurrent.CopyOnWriteArrayList;

public abstract class IScreenPlayer {

    /**
     * The applicable screen manager
     */
    protected ScreenManager manager;

    /**
     * The list of watchers
     */
    @Getter
    public CopyOnWriteArrayList<Player> watchers;

    /**
     * Sets the manager
     *
     * @param manager The manager
     */
    public void setManager(ScreenManager manager) {
        this.manager = manager;
        this.watchers = new CopyOnWriteArrayList<>();
    }

    /**
     * Adds a new watcher
     *
     * @param player The watcher
     */
    public void addWatcher(Player player) {
        initWatcher(player);
        watchers.add(player);
    }

    /**
     * Renders the screen
     */
    public abstract void render();

    /**
     * Initializes watcher for a player
     *
     * @param player The watcher
     */
    public abstract void initWatcher(Player player);

    /**
     * Stops the player
     */
    public abstract void stop(Player player);
}
