package dev.spaceseries.spaceholoplayer.screen.player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Registry;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Serializer;
import net.minecraft.server.v1_16_R1.IChatBaseComponent;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class ProtocolScreenPlayer extends IScreenPlayer {

    /**
     * The byte serializer
     */
    private final static Serializer byteSerializer = Registry.get(Byte.class);

    /**
     * The boolean serializer
     */
    private final static Serializer booleanSerializer = Registry.get(Boolean.class);

    /**
     * ID offset
     */
    private final int ID_OFFSET = 10000000;

    /**
     * List of watchers
     */
    private final List<UUID> uuids;

    /**
     * The protocol manager
     */
    private final ProtocolManager protocolManager;

    /**
     * Initializes new protocol player
     *
     * @param protocolManager The protocol manager
     */
    public ProtocolScreenPlayer(ProtocolManager protocolManager) {
        // initializes protocol manager
        this.protocolManager = protocolManager;

        // initializes watcher list
        uuids = new ArrayList<>();
    }

    /**
     * Renders the frames
     */
    @Override
    public void render() {
        List<String> frame = manager.getCache().getFrame();
        if (frame == null) {
            return;
        }

        for (int x = manager.getHeight() - 1; x >= 0; x--) {
            setRow(x, frame.get(x));
        }
    }

    /**
     * Initializes a watcher
     *
     * @param player The watcher
     */
    @Override
    public void initWatcher(Player player) {
        // loops through # of lines
        for (int id = manager.getHeight() - 1; id >= 0; id--) {
            UUID uuid = UUID.randomUUID();
            uuids.add(uuid);

            // spawns hologram
            spawnEntity(player,
                    manager.getScreenStart().getX(),
                    manager.getScreenStart().getZ(),
                    manager.getScreenStart().getY() - (id * 0.225),
                    id,
                    uuid);
        }
    }

    @Override
    public void stop(Player player) {
        // loops through # of lines
        for (int id = manager.getHeight() - 1; id >= 0; id--) {
            UUID uuid = UUID.randomUUID();
            uuids.add(uuid);

            // destroy hologram
            destroyEntity(player,
                    manager.getScreenStart().getX(),
                    manager.getScreenStart().getZ(),
                    manager.getScreenStart().getY() - (id * 0.225),
                    id,
                    uuid);
        }
    }

    /**
     * Sets the row of text in the hologram to display
     *
     * @param row  The row #
     * @param json The json string
     */
    public void setRow(int row, String json) {

        // create metadata packet to update entity (hologram)
        PacketContainer metaDataPacket = protocolManager.createPacket(PacketType.Play.Server.ENTITY_METADATA, true);

        // create watcher
        WrappedDataWatcher watcher = new WrappedDataWatcher();

        // set the object, serialize json display name
        watcher.setObject(2,
                WrappedDataWatcher.Registry.getChatComponentSerializer(true),
                Optional.ofNullable(IChatBaseComponent.ChatSerializer.jsonToComponent(json))); // sets display name

        // write ID offset
        metaDataPacket.getIntegers()
                .write(0, row + ID_OFFSET);

        // write watchable objects to packet
        metaDataPacket.getWatchableCollectionModifier()
                .write(0, watcher.getWatchableObjects());

        // send update packet
        try {
            for (Player player : watchers) {
                protocolManager.sendServerPacket(player, metaDataPacket);
            }
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Spawns the holograms at given location
     *
     * @param player The player
     * @param x      X coordinate
     * @param z      Z coordinate
     * @param y      Z coordinate
     * @param id     The id
     * @param uuid   The uuid
     */
    public void spawnEntity(Player player, double x, double z, double y, int id, UUID uuid) {
        // create spawn packet
        PacketContainer packet = protocolManager.createPacket(PacketType.Play.Server.SPAWN_ENTITY_LIVING);

        // write id offsets, uuids, etc
        packet.getIntegers()
                .write(0, id + ID_OFFSET)
                .write(1, 1);
        packet.getUUIDs()
                .write(0, uuid);
        packet.getDoubles() // coordinates
                .write(0, x)
                .write(1, y)
                .write(2, z);

        // create packet
        PacketContainer metaDataPacket = protocolManager.createPacket(PacketType.Play.Server.ENTITY_METADATA, true);

        // create watcher
        WrappedDataWatcher watcher = new WrappedDataWatcher();

        // write properties
        watcher.setObject(0, byteSerializer, (byte) (0x20));
        watcher.setObject(14, byteSerializer, (byte) (0x01 | 0x10)); // small marker
        watcher.setObject(3, booleanSerializer, Boolean.TRUE); // custom name visible
        watcher.setObject(5, booleanSerializer, Boolean.TRUE); // no gravity

        // write id offsets, etc
        metaDataPacket.getIntegers()
                .write(0, id + ID_OFFSET);
        metaDataPacket.getWatchableCollectionModifier()
                .write(0, watcher.getWatchableObjects());

        // send packets for spawning and entity modifications
        try {
            protocolManager.sendServerPacket(player, packet);
            protocolManager.sendServerPacket(player, metaDataPacket);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Spawns the holograms at given location
     *
     * @param player The player
     * @param x      X coordinate
     * @param z      Z coordinate
     * @param y      Z coordinate
     * @param id     The id
     * @param uuid   The uuid
     */
    public void destroyEntity(Player player, double x, double z, double y, int id, UUID uuid) {
        // create destroy packet
        PacketContainer packet = protocolManager.createPacket(PacketType.Play.Server.ENTITY_DESTROY);

        // write id offsets, uuids, etc
        packet.getIntegers()
                .write(0, id + ID_OFFSET)
                .write(1, 1);
        packet.getUUIDs()
                .write(0, uuid);
        packet.getDoubles() // coordinates
                .write(0, x)
                .write(1, y)
                .write(2, z);

        // send packets for destroying entity
        try {
            protocolManager.sendServerPacket(player, packet);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
