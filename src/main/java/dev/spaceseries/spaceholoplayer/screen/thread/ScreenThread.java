package dev.spaceseries.spaceholoplayer.screen.thread;

import dev.spaceseries.spaceholoplayer.screen.manager.ScreenManager;

public class ScreenThread extends Thread {

    /**
     * Is the thread running?
     */
    private boolean isRunning = true;

    /**
     * The applicable manager for the thread
     */
    private final ScreenManager manager;

    /**
     * Initializes new screen thread
     *
     * @param manager The applicable manager
     */
    public ScreenThread(ScreenManager manager) {
        this.manager = manager;
    }

    /**
     * Runs the encoder inside the thread
     */
    @Override
    public void run() {
        // while running
        while (this.isRunning) {
            // calculate target time
            long targetTime = System.currentTimeMillis() + (1000 / manager.getFps());

            // render frame
            manager.render();

            while (System.currentTimeMillis() < targetTime) {
                // encode next frame with this buffer
                manager.encode();
            }
        }
    }

    /**
     * Sets running status
     *
     * @param running If the thread is running
     */
    public synchronized void setRunning(boolean running) {
        isRunning = running;
    }
}