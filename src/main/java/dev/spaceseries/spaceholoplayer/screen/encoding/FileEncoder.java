package dev.spaceseries.spaceholoplayer.screen.encoding;

import dev.spaceseries.spaceholoplayer.SpaceHoloPlayer;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Java2DFrameConverter;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

public class FileEncoder extends FrameEncoder {

    /**
     * Name of the file
     */
    private final String fileName;

    /**
     * Frame converter
     */
    private final Java2DFrameConverter converter;

    /**
     * Frame grabber
     */
    private FFmpegFrameGrabber grabber;

    /**
     * Initializes frame encoder
     *
     * @param fileName The file name
     */
    public FileEncoder(String fileName) {
        this.fileName = fileName;
        this.converter = new Java2DFrameConverter();
    }

    @Override
    public void load() {
        // initialize grabber
        grabber = new FFmpegFrameGrabber(new File(SpaceHoloPlayer.getInstance().getDataFolder(), fileName));
        // set fps
        grabber.setFrameRate(manager.getFps());
        // set height
        grabber.setImageHeight(manager.getHeight());
        // set width
        grabber.setImageWidth(manager.getWidth());

        try {
            grabber.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    long time = 0;

    private void start() {
        time = System.currentTimeMillis();
    }

    public void time(String name) {
        System.out.println(name + ": " + (System.currentTimeMillis() - time));
        time = System.currentTimeMillis();
    }

    @Override
    public void encode() {
        try {
            start();

            // if the frame number is > than the maximum video frames, stop the encoding process
            if (grabber.getFrameNumber() >= grabber.getLengthInFrames()) {
                manager.stop();
                return;
            }

            BufferedImage frame = converter.getBufferedImage(grabber.grabImage());
            ArrayList<String> newFrame = new ArrayList<>(frame.getHeight());

            // cycles through the image pixels
            for (int y = 0; y < frame.getHeight(); y++) {
                StringBuilder builder = new StringBuilder("{\"extra\":[");

                for (int x = 0; x < frame.getWidth(); x++) {
                    int color = frame.getRGB(x, y);

                    builder.append("{\"color\":\"#");
                    builder.append(Integer.toHexString(color).substring(2));
                    // append pixel character
                    builder.append("\",\"text\":\"█\"},");
                }
                builder.deleteCharAt(builder.length() - 1);
                builder.append("],\"text\":\"\"}");

                newFrame.add(builder.toString());
                time("Row encoded in time " + time);
            }

            manager.getCache().addFrame(newFrame);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
