package dev.spaceseries.spaceholoplayer.screen.encoding;

import dev.spaceseries.spaceholoplayer.screen.manager.ScreenManager;

public abstract class FrameEncoder {

    protected ScreenManager manager;

    public abstract void load();

    public abstract void encode();

    public void setManager(ScreenManager manager) {
        this.manager = manager;
    }
}
