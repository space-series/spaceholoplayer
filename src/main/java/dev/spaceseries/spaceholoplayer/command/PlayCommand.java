package dev.spaceseries.spaceholoplayer.command;

import com.comphenix.protocol.ProtocolLib;
import com.yakovliam.spaceapi.command.*;
import dev.spaceseries.spaceholoplayer.SpaceHoloPlayer;
import dev.spaceseries.spaceholoplayer.screen.encoding.FileEncoder;
import dev.spaceseries.spaceholoplayer.screen.manager.ScreenManager;
import dev.spaceseries.spaceholoplayer.screen.player.ProtocolScreenPlayer;
import org.bukkit.entity.Player;

import static dev.spaceseries.spaceholoplayer.util.Messages.SHP_HELP;

@SubCommand
@PlayersOnly
public class PlayCommand extends Command {

    public PlayCommand() {
        super(SpaceHoloPlayer.getPlugin(), "play");
    }

    @Override
    public void onCommand(SpaceCommandSender sender, String s, String... args) {

        // if args length not 2, return
        if (args.length != 2) {
            SHP_HELP.msg(sender);
            return;
        }

        // get file name
        String fileName = args[0];

        // get fps
        int fps;
        try {
            fps = Integer.parseInt(args[1]);
        }catch (Exception ignored){
            SHP_HELP.msg(sender);
            return;
        }

        // get player
        Player player = (Player) ((BukkitSpaceCommandSender) sender).getBukkitSender();

        // play
        ScreenManager manager = new ScreenManager(256, 144, fps, player.getLocation(),
                new ProtocolScreenPlayer(SpaceHoloPlayer.getInstance().getProtocolManager()), new FileEncoder(fileName), true);

        manager.addWatcher(player);

        manager.start();
    }
}
