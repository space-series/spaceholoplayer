package dev.spaceseries.spaceholoplayer.command;

import com.yakovliam.spaceapi.command.Command;
import com.yakovliam.spaceapi.command.PlayersOnly;
import com.yakovliam.spaceapi.command.SpaceCommandSender;
import dev.spaceseries.spaceholoplayer.SpaceHoloPlayer;

import java.util.Collections;

import static dev.spaceseries.spaceholoplayer.util.Messages.SHP_HELP;

@PlayersOnly
public class SpaceHoloPlayerCommand extends Command {

    public SpaceHoloPlayerCommand() {
        super(SpaceHoloPlayer.getPlugin(),
                "spaceholoplayer",
                "Command",
                Collections.singletonList("shp"));

        addSubCommands(
                new PlayCommand()
        );
    }

    @Override
    public void onCommand(SpaceCommandSender sender, String s, String... args) {
        SHP_HELP.msg(sender);
    }
}
