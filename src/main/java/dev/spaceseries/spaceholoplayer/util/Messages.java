package dev.spaceseries.spaceholoplayer.util;

import com.yakovliam.spaceapi.text.Message;

public class Messages {

    public static final Message SHP_HELP = Message.builder("space.holoplayer.help")
            .addLine("&3&lSpaceHoloPlayer")
            .addLine("&7 - /shp - This page")
            .addLine("&7 - /shp play <filename> <fps> - Creates a screen at your location and plays the specified video")
            .build();
}
