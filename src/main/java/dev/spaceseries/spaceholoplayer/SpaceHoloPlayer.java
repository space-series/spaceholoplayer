package dev.spaceseries.spaceholoplayer;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.yakovliam.spaceapi.abstraction.plugin.BukkitPlugin;
import dev.spaceseries.spaceholoplayer.command.SpaceHoloPlayerCommand;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

public final class SpaceHoloPlayer extends JavaPlugin {

    /**
     * Instance of main class
     */
    @Getter
    private static SpaceHoloPlayer instance;

    /**
     * Protocol Manager
     */
    @Getter
    private ProtocolManager protocolManager;

    /**
     * The Bukkit plugin SpaceAPI
     */
    @Getter
    private static BukkitPlugin plugin;

    @Override
    public void onEnable() {
        instance = this;

        // initialize api
        plugin = new BukkitPlugin(this);

        // init commands
        new SpaceHoloPlayerCommand();

        // initialize protocol manager
        protocolManager = ProtocolLibrary.getProtocolManager();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
